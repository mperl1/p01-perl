//
//  ViewController.h
//  Hello World
//
//  Created by Matt Perl on 1/23/17.
//  Copyright © 2017 Matt Perl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *changeAndMoveButton;
@property (strong, nonatomic) IBOutlet UILabel *helloWorldLabel;


//-(IBAction) clickResponse:(id)sender;
//-(IBAction) clickResponse2:(id)sender;
//-(IBAction) clickResponse3:(id)sender;
//-(IBAction) clickResponse4:(id)sender;
@end

