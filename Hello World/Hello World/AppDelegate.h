//
//  AppDelegate.h
//  Hello World
//
//  Created by Matt Perl on 1/23/17.
//  Copyright © 2017 Matt Perl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

