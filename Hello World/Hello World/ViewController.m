//
//  ViewController.m
//  Hello World
//
//  Created by Matt Perl on 1/23/17.
//  Copyright © 2017 Matt Perl. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

static int count = 0;

- (IBAction)clickResponse:(id)sender {
    CGRect buttonFrame = _changeAndMoveButton.frame;
    
    if(count == 0){
        [_changeAndMoveButton setTitle:@"Missed me!" forState:UIControlStateNormal];
        buttonFrame.origin.x = 90;
        buttonFrame.origin.y = 150;
        _changeAndMoveButton.frame = buttonFrame;
        ++count;
    }
    else if(count == 1){
        [_changeAndMoveButton setTitle:@"You're not very good at this..." forState:UIControlStateNormal];
        buttonFrame.origin.x = 180;
        buttonFrame.origin.y = 250;
        _changeAndMoveButton.frame = buttonFrame;
        ++count;
    }
    else if(count == 2){
        [_changeAndMoveButton setTitle:@"That was kind of close...I guess.." forState:UIControlStateNormal];
        buttonFrame.origin.x = 50;
        buttonFrame.origin.y = 150;
        _changeAndMoveButton.frame = buttonFrame;

        ++count;
    }
    else if(count == 3){
        [_changeAndMoveButton setTitle:@"There ya go!" forState:UIControlStateNormal];
        [_helloWorldLabel setText:@"Matt Perl completed the first assignment."];
        //_helloWorldLabel.textAlignment = NSTextAlignmentCenter;
    }
}



@end
